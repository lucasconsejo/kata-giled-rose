# Kata Gilded Rose
Par Lucas Consejo & Guillaume Anton

Techno utilisée : Javascript  
Framework de test : Jest  

Choix du Kata :  
- Choix personnel
- Règles plus compréhensibles

## Stratégie

- Utilisation de snapshots pour réfactorer sans impacter les fonctionnalités existantes

## Liste des améliorations
- Définir des classes différentes pour chaque produit (afin de gérer leur comportement indépendamment). Principe du SRP.

- La methode updateQuality dans Shop sera gérer dans chacune des classes produits
(Ex: La classe Aged Brie aura la methode updateQuality qui aura un comportement différent que le updateQuality dans la classe Sulfuras)

- Refacto du updateQuality
(Séparer en plusieurs méthodes pour diminuer le nombre de ligne dans la boucle for)

- Refacto de code smells (Méthode longue, code dupliqué, nombre magique, KISS, etc...) 

- Le fait de refactorer ainsi, nous permettra d'implémenter plus facilement la nouvelle fonctionnalité Conjured

- Corriger le test en erreur dans le fichier gilded_rose.js

- Ajouter des tests unitaires une fois la refacto terminé

## Priorisation
- Création de snapshot
- Création de classe pour chaque produit qui extends de la classe Item
- Refactor code smells
- Ajout de la nouvelle fonctionnalité
- Override de la classe updateQuality dans chaque classe produit
- Refactor de la méthode updateQuality 
- Ajouter des tests unitaires
