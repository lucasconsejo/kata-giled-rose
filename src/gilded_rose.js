const AgedBrie = require("./items/agedBrie");
const Backstage = require("./items/backstage");
const ClassicItems = require("./items/classicitems");
const Conjured = require("./items/conjured");
const Sulfuras = require("./items/sulfuras");

class Shop {
  constructor(items=[]){
    this.items = items.map((item) => {
      if (item.name == 'Aged Brie') return new AgedBrie(item.sellIn, item.quality);
      else if (item.name == 'Backstage passes to a TAFKAL80ETC concert') return new Backstage(item.sellIn, item.quality);
      else if (item.name == 'Conjured Mana Cake') return new Conjured(item.sellIn, item.quality);
      else if (item.name == 'Sulfuras, Hand of Ragnaros') return new Sulfuras();
      else return new ClassicItems(item.name, item.sellIn, item.quality);
    });
  }

  updateQuality() {
    this.items.forEach((item) => item.update());
    return this.items;
  }
}

module.exports = Shop
