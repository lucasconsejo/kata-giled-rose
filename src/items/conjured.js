const Item = require("./item");

class Conjured extends Item {
  constructor(sellIn, quality) {
    super('Conjured Mana Cake', sellIn, quality);
  }

  update() {
    this.sellIn = this.sellIn - 1;
    this.quality = (this.sellIn > 0) ? this.quality -= 2 : this.quality -= 4;
    if (this.quality > 50) this.quality = 50;
    if (this.quality < 0) this.quality = 0;
  }
}

module.exports = Conjured;