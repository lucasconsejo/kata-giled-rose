const Item = require("./item");

class ClassicItems extends Item {
  constructor(name, sellIn, quality) {
    super(name, sellIn, quality);
  }

  update() {
    this.sellIn = this.sellIn - 1;
    this.quality = (this.sellIn > 0) ? this.quality -= 1 : this.quality -= 2;
    if (this.quality > 50) this.quality = 50;
    if (this.quality < 0) this.quality = 0;
  }
}

module.exports = ClassicItems;