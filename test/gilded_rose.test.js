const Shop= require("../src/gilded_rose");
const Item = require("../src/items/item");

describe("Gilded Rose", function() {
  it("should foo correctly", function() {
    const gildedRose = new Shop([new Item("foo", 10, 20)]);
    const items = gildedRose.updateQuality();
    expect(items[0].name).toBe("foo");
    expect(items[0].sellIn).toBe(9);
    expect(items[0].quality).toBe(19);
  });
  it("should foo without negative quality", function() {
    const gildedRose = new Shop([new Item("foo", 0, 0)]);
    const items = gildedRose.updateQuality();
    expect(items[0].name).toBe("foo");
    expect(items[0].sellIn).toBe(-1);
    expect(items[0].quality).toBe(0);
  });
  it("should foo with expired date", function() {
    const gildedRose = new Shop([new Item("foo", -1, 2)]);
    const items = gildedRose.updateQuality();
    expect(items[0].name).toBe("foo");
    expect(items[0].sellIn).toBe(-2);
    expect(items[0].quality).toBe(0);
  });
  it("should foo without quality greater than 50", function() {
    const gildedRose = new Shop([new Item("foo", 1, 80)]);
    const items = gildedRose.updateQuality();
    expect(items[0].name).toBe("foo");
    expect(items[0].sellIn).toBe(0);
    expect(items[0].quality).toBe(50);
  });
  it("should Aged Brie correctly", function() {
    const gildedRose = new Shop([new Item("Aged Brie", 2, 0)]);
    const items = gildedRose.updateQuality();
    expect(items[0].name).toBe("Aged Brie");
    expect(items[0].sellIn).toBe(1);
    expect(items[0].quality).toBe(1);
  });
  it("should Aged Brie with expired date", function() {
    const gildedRose = new Shop([new Item("Aged Brie", -1, 3)]);
    const items = gildedRose.updateQuality();
    expect(items[0].name).toBe("Aged Brie");
    expect(items[0].sellIn).toBe(-2);
    expect(items[0].quality).toBe(5);
  });
  it("should Aged Brie without quality greater than 50", function() {
    const gildedRose = new Shop([new Item("Aged Brie", 2, 80)]);
    const items = gildedRose.updateQuality();
    expect(items[0].name).toBe("Aged Brie");
    expect(items[0].sellIn).toBe(1);
    expect(items[0].quality).toBe(50);
  });
  it("should Backstage correctly", function() {
    const gildedRose = new Shop([new Item("Backstage passes to a TAFKAL80ETC concert", 14, 9)]);
    const items = gildedRose.updateQuality();
    expect(items[0].name).toBe("Backstage passes to a TAFKAL80ETC concert");
    expect(items[0].sellIn).toBe(13);
    expect(items[0].quality).toBe(10);
  });
  it("should Backstage with the quality increasing by 3", function() {
    const gildedRose = new Shop([new Item("Backstage passes to a TAFKAL80ETC concert", 5, 2)]);
    const items = gildedRose.updateQuality();
    expect(items[0].name).toBe("Backstage passes to a TAFKAL80ETC concert");
    expect(items[0].sellIn).toBe(4);
    expect(items[0].quality).toBe(5);
  });
  it("should Backstage with the quality increasing by 2", function() {
    const gildedRose = new Shop([new Item("Backstage passes to a TAFKAL80ETC concert", 8, 2)]);
    const items = gildedRose.updateQuality();
    expect(items[0].name).toBe("Backstage passes to a TAFKAL80ETC concert");
    expect(items[0].sellIn).toBe(7);
    expect(items[0].quality).toBe(4);
  });
  it("should Backstage with quality 0", function() {
    const gildedRose = new Shop([new Item("Backstage passes to a TAFKAL80ETC concert", -2, 10)]);
    const items = gildedRose.updateQuality();
    expect(items[0].name).toBe("Backstage passes to a TAFKAL80ETC concert");
    expect(items[0].sellIn).toBe(-3);
    expect(items[0].quality).toBe(0);
  });
  it("should Backstag without quality greater than 50", function() {
    const gildedRose = new Shop([new Item("Backstage passes to a TAFKAL80ETC concert", 2, 80)]);
    const items = gildedRose.updateQuality();
    expect(items[0].name).toBe("Backstage passes to a TAFKAL80ETC concert");
    expect(items[0].sellIn).toBe(1);
    expect(items[0].quality).toBe(50);
  });
  it.each`
		sellIn | quality
		${0} | ${0}
    ${0} | ${5}
    ${0} | ${-5}
    ${2} | ${0}
    ${-2} | ${0}
    ${2} | ${-5}
    ${-5} | ${2}
    ${-5} | ${100}
	`("should Sulfuras always return sellIn 0 and quality 80", function({sellIn, quality}) {
    const gildedRose = new Shop([new Item("Sulfuras, Hand of Ragnaros", sellIn, quality)]);
    const items = gildedRose.updateQuality();
    expect(items[0].name).toBe("Sulfuras, Hand of Ragnaros");
    expect(items[0].sellIn).toBe(0);
    expect(items[0].quality).toBe(80);
  });
  it("should Conjured correctly", function() {
    const gildedRose = new Shop([new Item("Conjured Mana Cake", 10, 20)]);
    const items = gildedRose.updateQuality();
    expect(items[0].name).toBe("Conjured Mana Cake");
    expect(items[0].sellIn).toBe(9);
    expect(items[0].quality).toBe(18);
  });
  it("should Conjured without negative quality", function() {
    const gildedRose = new Shop([new Item("Conjured Mana Cake", 0, 0)]);
    const items = gildedRose.updateQuality();
    expect(items[0].name).toBe("Conjured Mana Cake");
    expect(items[0].sellIn).toBe(-1);
    expect(items[0].quality).toBe(0);
  });
  it("should Conjured without quality greater than 50", function() {
    const gildedRose = new Shop([new Item("Conjured Mana Cake", 0, 80)]);
    const items = gildedRose.updateQuality();
    expect(items[0].name).toBe("Conjured Mana Cake");
    expect(items[0].sellIn).toBe(-1);
    expect(items[0].quality).toBe(50);
  });
  it("should Conjured with expired date", function() {
    const gildedRose = new Shop([new Item("Conjured Mana Cake", -1, 4)]);
    const items = gildedRose.updateQuality();
    expect(items[0].name).toBe("Conjured Mana Cake");
    expect(items[0].sellIn).toBe(-2);
    expect(items[0].quality).toBe(0);
  });
  it("should empty array", function() {
    const gildedRose = new Shop();
    const items = gildedRose.updateQuality();
    expect(items.length).toBe(0);
  });
  it("should return mutli items", function() {
    const gildedRose = new Shop([
      new Item("foo", 2, 3),
      new Item("Aged Brie", 5, 2),
      new Item("Conjured Mana Cake", 4, 10),
    ]);
    const items = gildedRose.updateQuality();
    expect(items.length).toBe(3);
    expect(items[0].name).toBe("foo");
    expect(items[0].sellIn).toBe(1);
    expect(items[0].quality).toBe(2);
    expect(items[1].name).toBe("Aged Brie");
    expect(items[1].sellIn).toBe(4);
    expect(items[1].quality).toBe(3);
    expect(items[2].name).toBe("Conjured Mana Cake");
    expect(items[2].sellIn).toBe(3);
    expect(items[2].quality).toBe(8);
  });
});
